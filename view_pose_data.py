import cv2
import os
import numpy as np


edges = [[0, 1], [1, 2], [2, 3], [3, 4], [0, 5], [5, 6], [6, 7], [7, 8], [0, 9], [9, 10], \
         [10, 11], [11, 12], [0, 13], [13, 14], [14, 15], [15, 16], [0, 17], [17, 18], [18, 19], [19, 20]]


def view_data():
    folders = os.listdir("Pose/")
    folders.sort()

    for f in folders:
        imgs = os.listdir("Pose/"+f+"/images")
        points = os.listdir("Pose/"+f+"/points")
        imgs.sort()
        points.sort()

        for i in range(len(imgs)):
            x = cv2.imread("Pose/"+f+"/images/" + imgs[i])
            y = np.loadtxt("Pose/"+f+"/points/" + points[i])

            for e in edges:
                x1, y1 = y[e[0]]
                x2, y2 = y[e[1]]
                x1 = int(x1)
                y1 = int(y1)
                x2 = int(x2)
                y2 = int(y2)
                cv2.line(x, (x1, y1), (x2, y2), (0, 255, 0), thickness=2)

            cv2.imshow("img", x)
            key = cv2.waitKey(0)
            # Use break if you want to skip through folders
            # if key == 27: break
            if key == 27: return


if __name__ == "__main__":
    view_data()
